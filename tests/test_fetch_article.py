from src.lib import fetch_article_oup
import unittest
import asyncio

class TestFetch(unittest.TestCase):
    def test_a_esr_open_access1(self):
        loop = asyncio.get_event_loop()
        paper1 = loop.run_until_complete(fetch_article_oup("https://academic.oup.com/esr/article/37/5/695/6298529"))
        
        self.assertEqual(paper1.doi, "https://doi.org/10.1093/esr/jcab010")
        self.assertEqual(paper1.title, "The Missing Link: Network Influences on Class Divides in Political Attitudes")
        self.assertEqual(paper1.authors, [ "Arvid Lindh", "Anton B Andersson", "Beate Volker"])
        self.assertEqual(paper1.journal, "European Sociological Review")
        self.assertEqual(paper1.issue, "5")
        self.assertEqual(paper1.volume, "37")
        self.assertEqual(paper1.abstract, "Previous research provides a detailed picture of class differences in political attitudes. Less is however known about the social structures that enforce this political divide across social classes. This article contributes towards filling this gap by considering how the class profile of personal social networks influences political attitudes. We propose a general framework for incorporating an individual\u2019s social network into class analysis of political preferences. Using Sweden as a case, we empirically evaluate our approach using a population survey with information about the respondents\u2019 own employment situation, egocentric networks, and political attitudes in terms of redistribution and welfare chauvinism. We find that there is considerable class segregation in social networks as individuals tend to have more ties within their own and neighbouring class positions. Concerning political preferences, results show that: (i) a substantive part of the class\u2013attitude relationship is shaped by a person\u2019s social network; (ii) the class profile of networks influences attitudes over and above one\u2019s own class position; (iii) class segregation in networks fortifies class divides in political attitudes. We thus conclude that social networks constitute a (hitherto) \u2018missing link\u2019 in class analysis of political preferences that merits careful consideration in theoretical models of contemporary politics.")
        self.assertEqual(paper1.sections[0]["title"], "Introduction")
        self.assertEqual(paper1.sections[0]["paragraphs"][0], "As substantial class divides in political attitudes persist, social class is of continuing political relevance among the general population (Evans and Tilley, 2017; Manza and Crowley, 2018; Lindh and McCall, 2020). Traditionally, sociological class analysis has discussed how class relations in the labour market and the workplace are reflected in personal social ties and contribute to social community and segregation beyond employment, and what the implications might be for class-based political mobilization (e.g. Goldthorpe et al., 1968; Wright, 1997). However, most research considers the micro-foundations of class politics only through the lens of an individual\u2019s own employment situation. While this research provides indispensable insights, a sizable share of the relationship between class position and political outcomes remains \u2018unexplained\u2019 after accounting for the immediate employment situation. Consequently, some studies conclude by speculating that social networks might be the \u2018missing link\u2019 that can disentangle the relationship between social class and political preferences (Weakliem and Heath, 1994; Brooks and Svallfors, 2010; Langs\u00e6ther and Evans, 2020). This article explores this proposition both theoretically and empirically using survey data.")
        self.assertEqual(len(paper1.sections), 9)
        self.assertEqual(len(paper1.references), 52)
        
    def test_b_esr_open_access2(self):
        loop = asyncio.get_event_loop()
        paper2 = loop.run_until_complete(fetch_article_oup("https://academic.oup.com/esr/article/37/5/731/6154325"))
        
        self.assertEqual(paper2.doi, "https://doi.org/10.1093/esr/jcab002")
        self.assertEqual(paper2.title, "Thirty Years after the Fall of the Berlin Wall—Do East and West Germans Still Differ in Their Attitudes to Female Employment and the Division of Housework?")
        self.assertEqual(paper2.authors, [ "Gundula Zoch"])
        self.assertEqual(paper2.journal, "European Sociological Review")
        self.assertEqual(paper2.issue, "5")
        self.assertEqual(paper2.volume, "37")
        self.assertEqual(paper2.abstract, "Previous cross-sectional studies highlight persistent East–West differences in gender ideologies after German reunification. This study examines the extent to which gender ideologies in the East and West have converged and whether differences are still relevant for younger cohorts who experienced childhood around the time of reunification, or after 1989. Using data from the German Family Panel pairfam (2008–2019) and differences in regime-specific socialization for three cohorts born before and after reunification, results reveal that different dimensions of gender ideologies have only partly converged 30 years after reunification. Attitudes towards housework and female employment converged particularly, yet, in all cohorts, views on maternal employment remain substantially different between East and West. Observed convergence occurred only partly due to contrasting trends of modernization in West Germany and re-traditionalization in East Germany. Moreover, the results highlight smaller attitude changes with increasing age, particularly for the younger cohorts, contributing to further variations in East–West differences. Overall, the findings confirm the existence of long-lasting ideology differences due to regime-specific socialization, and a persistently altered composition of society in East and West Germany. At the same time, they point towards slow convergence among younger cohorts due to a more similar institutional and socialization context following reunification.")
        self.assertEqual(paper2.sections[0]["title"], "Introduction")
        self.assertEqual(len(paper2.sections), 6)
        self.assertEqual(len(paper2.references), 30)
        
if __name__ == "__main__":
    unittest.main()