# shell.nix
{ pkgs ? import <nixpkgs> {} }:
with pkgs;
with python39Packages;
let
  my-python = pkgs.python3;
  charset-normalizer0 = python39.pkgs.buildPythonPackage rec {
    pname = "charset-normalizer";
    version = "2.0.0";
    src = python38.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "878bfe99324a9f3191e9a0f555e52ca2e0e1ad14b69802bc3f9a7afc71ffcfa4";
    };
    doCheck = false;
  };
  crossrefapi = python39.pkgs.buildPythonPackage rec {
    pname = "crossrefapi";
    version = "1.5.0";
    src = python39.pkgs.fetchPypi {
      inherit pname version;
      sha256 = "db4b688d97ec624a243e52532e0ade6172185d2a778c9c7bc458b7ee129bb884";
    };
    buildInputs = [ requests ];
    doCheck = false;
  };
  spacy-en_core_web_sm = python39.pkgs.buildPythonPackage rec {
    pname = "en_core_web_sm";
    version = "3.1.0";
    src = pkgs.fetchurl {
      url = "https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-3.1.0/en_core_web_sm-3.1.0-py3-none-any.whl";
      sha256 = "82bc8de1e7fa6609f0ff481af519c18291d0204044580df0813461608ae2d00c";
    };
    format = "wheel";
    doCheck = false;
    buildInputs = [ spacy ];
  };
  python-with-my-packages = my-python.withPackages (p: with p; [
    pandas
    requests
    beautifulsoup4
    pip
    unidecode
    wcwidth
    crossrefapi
    ipykernel
    spacy-en_core_web_sm
    spacy
    spacy-transformers
    pyuseragents
    jupyter
    jupyterlab
    transformers
    sentencepiece
    pytorch
    tensorflow
  ]);
in
pkgs.mkShell {
  buildInputs = [
    python-with-my-packages
    crossrefapi
  ];
  shellHook = ''
    PYTHONPATH=${python-with-my-packages}/${python-with-my-packages.sitePackages}
    
    TEMPDIR=$(mktemp -d -p /tmp)
    mkdir -p $TEMPDIR
    cp -r ${pkgs.python39Packages.jupyterlab}/share/jupyter/lab/* $TEMPDIR
    chmod -R 755 $TEMPDIR
    echo "Jupyter $TEMPDIR is the app directory"
    echo "to start jupyter, type 'jupyter lab --app-dir=$TEMPDIR"
  '';
}

# To start Jupyter:
# jupyter lab --app-dir=$TEMPDIR