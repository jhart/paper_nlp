#%%
import spacy
import pandas as pd 
from os import getcwd 
from spacy import displacy

# Read data
path = getcwd() 
file = path + "/../data/csv/papers.csv"
df = pd.read_csv(file)

# Get methods sections
methods = df[df["section"].str.lower().str.find("method") > 0]
text = methods.loc[:, "paragraph"].values[10]

# Linguistic annotations
# for model: python -m spacy download en_core_web_sm
nlp = spacy.load("en_core_web_sm")

doc = nlp(text)
displacy.render(doc, style="ent")

