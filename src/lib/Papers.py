# import logging first to start logging
from lib import Logger
import logging
# import own modules next
from .Paper import Paper
# import general modules
import os
import getpass
from configparser import ConfigParser


# start the logger
logger = logging.getLogger(__name__)
logger.addHandler(Logger.start_log_streaming())

# get config info
config = ConfigParser()
config.read("config.ini")


class Papers:
    def __init__():
        pass

    def readFolder():
        """Returns an array of paper file names in data/papers/"""
        f = []
        for (dirpath, dirnames, filenames) in os.walk(config[getpass.getuser()]["folder_papers"]):
            f.extend(filenames)
            break
        return f

    def count():
        """Returns the number of papers stored in data/papers/"""
        paper_count = len(Papers.readFolder())

        logger.info(
            f"You have {paper_count} papers with manuscript data in store.")

        return paper_count

    def toDataFrame():
        """Create a pandas DataFrame from all saved papers"""
        entries = None

        for file in Papers.readFolder():
            filename = os.getcwd() + "/" + \
                config[getpass.getuser()]["folder_papers"] + file
            p = Paper.load(filename)
            row = p.toDataFrame()
            if entries is None:
                entries = row
            else:
                entries = entries.append(row)
        return entries

    def createTrainingData(keyword):
        """Creates training text data for a section that matches the keyword"""
        print("Create training data from papers")

        f = "data/nlp_models/training_data_" + keyword + ".txt"

        with open(f, "w") as outfile:
            outfile.write("")

        for file in Papers.readFolder():
            filename = os.getcwd() + "/" + \
                config[getpass.getuser()]["folder_papers"] + file
            p = Paper.load(filename)
            for section in p.sections:
                if keyword.lower() in section["title"].lower():
                    for paragraph in section["paragraphs"]:
                        with open(f, "a") as outfile:
                            outfile.write(paragraph + "\n\n")
