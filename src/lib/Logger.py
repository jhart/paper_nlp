import logging
import os
from datetime import datetime
from configparser import ConfigParser
import getpass

# get config info
config = ConfigParser()
config.read("config.ini")


def delete_logs(all=False) -> None:
    """This function deletes old logs. Either all or all but the last."""
    log_list = os.listdir(config[getpass.getuser()]['folder_logs'])
    log_list.sort()
    print(log_list)
    if (all == False):
        log_list = log_list[:-2]
    else:
        log_list = log_list[:-1]
    for l in log_list:

        os.remove(f"{config[getpass.getuser()]['folder_logs']}{l}")


logger = logging.getLogger(__name__)
# defining logging format
logformat = "%(levelname)s:%(name)s - %(asctime)s - %(message)s"
loglevel = logging.DEBUG
# setting basic configuration of the logger
logging.basicConfig(filename=f"{config[getpass.getuser()]['folder_logs']}{datetime.now().strftime('%Y_%m_%d_T%H_%M_%S')}.log",
                    level=loglevel,
                    format=logformat,
                    filemode="w")


def start_log_streaming() -> logging.StreamHandler():
    """This function can be called to set tthe stream_handler in other modules."""
    streamformat = logging.Formatter("%(levelname)s - %(name)s - %(message)s")
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(streamformat)
    stream_handler.setLevel(loglevel)

    return stream_handler


logger.setLevel(loglevel)
logger.debug(f"Formats defined")

logger.addHandler(start_log_streaming())

logger.info(f"Program starts!")


# print(logging.DEBUG + 3)
# test the logger
# logger.debug("Harmless debug message.")
# logger.info("This is a test log.")
# logger.warning("We should do something about 0 in days here.")
# logger.error("This is plain wrong.")
# logger.critical("You are using SPSS.")
# numerical values of different logger types
# NOTSET         0
# DEBUG			10
# INFO			20
# WARNING		30
# ERROR			40
# CRITICAL		50
