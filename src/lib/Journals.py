# import logging first to start logging
from lib import Logger
import logging
# import own modules next
from .Journal import Journal
from .Paper import Paper
# import general modules
import os
import getpass
from configparser import ConfigParser

# start the logger
logger = logging.getLogger(__name__)
logger.addHandler(Logger.start_log_streaming())

# get config info
config = ConfigParser()
config.read("config.ini")


class Journals:
    def __init__(self):
        pass

    def fetchAll(journals):
        for journal in journals:
            res = Journal(issn=journal["issn"])
            logger.debug(f"Results are fetched for journal {journal}")
            res.fetchJournalPapers(journal["years"][0], journal["years"][1])
            logger.debug(
                f"{config[getpass.getuser()]['folder_journals']}{res.title.lower().replace(' ', '_')}.json")
            file = f"{config[getpass.getuser()]['folder_journals']}{res.title.lower().replace(' ', '_')}.json"
            res.save(file)

    def readFolder():
        f = []
        for (dirpath, dirnames, filenames) in os.walk(config[getpass.getuser()]["folder_journals"]):
            f.extend(filenames)
            break
        return f

    def savePapers(journal):
        if journal:
            # try:
            journal_file = Journal.readFile(journal)
            logger.info(
                f"Data loaded from JSON file of journal {journal}")
            # print(journal_file)
            for p in journal_file["papers"]:
                # print(p)
                paper = Paper(doi=p["URL"])
                logger.info(
                    f"Paper {p['URL']} is scraped.")
                paper.save()
            # except:
            #print("Didn't find journal", journal)
            #logger.error(f"Didn't find journal {journal}")
        else:
            for file in Journals.readFolder():
                journal_file = Journal.readFile(file)
                for p in journal_file["papers"]:
                    paper = Paper(doi=p["URL"])
                    paper.save()
