# import logging first to start logging
from lib import Logger
import logging
# import own modules next
from lib import FetchPublishers
# import general modules
from bs4 import BeautifulSoup
import json
import requests
import unidecode
from crossref.restful import Works, Journals
import os
import getpass
import pandas as pd
import pyuseragents
from configparser import ConfigParser

# get config info
config = ConfigParser()
config.read("config.ini")

# start the logger
logger = logging.getLogger(__name__)
logger.addHandler(Logger.start_log_streaming())

works = Works()
journals = Journals()

config[getpass.getuser(
)]["folder_papers"] = "/Users/bootstrap/Documents/portfolio/paper_nlp/data/papers/"
config[getpass.getuser(
)]["folder_html"] = "/Users/bootstrap/Documents/portfolio/paper_nlp/data/html/"

# introduce a fetchclass with methods for different publishers
# fetchContent.Sage(self, URL)


class Paper(FetchPublishers.Mixin):
    """Generates a Paper instance which scrapes the bibliographic information and the complete content of the article."""

    def __init__(self, doi="", title="", authors=[], journal="", issue="", volume="", year="",
                 abstract="", sections=[], references=[], url="", issn="", publisher=""):
        self.doi = doi
        self.abstract = abstract
        self.sections = sections
        self.references = references

        if doi != "":
            logger.debug("Paper class is called and DOI is available.")
            # if not self.checkFileExists():
            self.fetchCrossrefInfo()
            # get content
            self.fetchContent()
        else:
            logger.debug("Paper class is called and DOI is NOT available.")
            self.title = title
            self.authors = authors
            self.journal = journal
            self.issue = issue
            self.volume = volume
            self.year = year
            self.url = url
            self.issn = issn

    # is an abtract method useful here?
    def fetchContent(self):
        """This function calls specific content fetchers absed on the publishers."""
        logger.debug("fetchContent is called.")
        # --- Get HTML soup --- #
        if not self.checkHTMLFileExists():
            logger.info(f"Fetching paper: {self.doi}")
            soup = self.fetchHTML()
        else:
            logger.info(
                f"HTML file for {self.doi} exists, reading its content.")
            filename = self.generateHTMLFileName()
            with open(filename) as fp:
                soup = BeautifulSoup(fp, 'html.parser')

        # check which publisher it is and call the appropriate method #
        # --- Parse the content from HTML --- #
        # try:
        logger.debug(
            "HTML parsing function for specific publisher is called.")
        if self.publisher == "SAGE Publications":
            self.fetchContentSAGE(soup)
        elif self.publisher == "Oxford University Press (OUP)":
            self.fetchContentOUP(soup)
        else:
            # raise Exception()
            logger.warning(
                f"Publisher {self.publisher} not yet implemented.")
        # except:

    def fetchCrossrefInfo(self) -> None:
        """Fetches title, issue, volume, year, issn and journal from Crossref"""
        logger.debug(f"fetchCrossrefInfo is called.")
        # check if info get be retrieved
        try:
            info = works.doi(self.doi)
        except:
            logger.warning(
                f"Couldn't fetch Crossref data for: {self.doi}")
        # if there is infor, store it in the instance
        if info:
            # list with all info to be stored
            output = ["title", "issue", "volume",
                      "year", "issn", "journal", "publisher"]
            # list with places in the dictionary where info is stored
            input = [unidecode.unidecode(
                info["title"][0]), info.get("issue"), info.get("volume"),
                info.get("published-print", {"date-parts": [[None]]})["date-parts"][0][0], info.get("ISSN")[0], journals.journal(issn=info.get("ISSN")[0])["title"], info.get("publisher")]
            # loop over input/output and put in None if it cannot be found without throwing an error
            for inp, out in zip(input, output):
                if inp:
                    setattr(self, out, inp)
                else:
                    setattr(self, out, None)
                    logger.info(f"{out} could not be extracted.")

    def fetchHTML(self):
        logger.debug("fetchHTML is called.")
        headers = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            # "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36"
            "User-Agent": pyuseragents.random()
        }

        try:
            page = requests.get(self.doi, headers=headers)
            if page.status_code != 200:
                logger.warning(f"Page status code: {page.status_code}")
                raise Exception()
            soup = BeautifulSoup(page.content, "html.parser")

            filename = Paper.generateHTMLFileName(self)

            with open(filename, "w") as outfile:
                outfile.write(str(soup))

            return soup
        except:
            logger.warning(f"Couldn't fetch url: {self.doi}")

    def generateHTMLFileName(self):
        """Generates the filename for the raw html file"""
        return config[getpass.getuser()]["folder_html"] + self.doi.replace("http://dx.doi.org/", "").replace("/", "_") + ".html"

    def generateFilename(self):
        """Generates the filename for the paper"""
        return config[getpass.getuser()]["folder_papers"] + self.doi.replace("http://dx.doi.org/", "").replace("/", "_") + ".json"

    def checkFileExists(self):
        """Checks if a json file for the doi already exists"""
        filename = self.generateFilename()
        if os.path.isfile(filename):
            logger.info(
                f"File for {self.doi} already exists")
            return True

    def checkHTMLFileExists(self):
        """Checks if a HTML file for the doi already exists"""
        filename = Paper.generateHTMLFileName(self)
        if os.path.isfile(filename):
            return True

    def save(self, append=False):
        """Saves a paper to data/papers/ in JSON format"""
        json_object = self.toJson()
        filename = self.generateFilename()

        logger.debug(
            f"save: {self.doi} has {len(self.sections)} sections.")

        # Only save when we have manuscript text data
        if len(self.sections) > 0:
            logger.info(
                f"Saving {self.doi} to: {filename}")

            with open(filename, "w") as outfile:
                outfile.write(json_object)

        else:
            try:
                filename = Paper.generateHTMLFileName(self)
                os.remove(filename)
                logger.warning(
                    f"No manuscript data for {self.doi}: not saving, removing HTML file.")
            except:
                pass

    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__, indent=4)

    def load(file):
        """Loads a paper from a json file"""
        with open(file) as json_file:
            data = json.load(json_file)

        p = Paper()
        try:
            p.title = data["title"]
        except:
            logger.warning(
                f"Couldn't read <title> for {json_file}")
            pass
        try:
            # p.authors = data["authors"]
            pass
        except:
            # print("Couldn't read authors")
            pass
        try:
            p.journal = data["journal"]
        except:
            logger.warning(
                f"Couldn't read <journal> for {json_file}")
            pass
        try:
            p.issue = data["issue"]
        except:
            logger.warning(
                f"Couldn't read <issue> for {json_file}")
            pass
        try:
            p.volume = data["volume"]
        except:
            logger.warning(
                f"Couldn't read <volume> for {json_file}")
            pass
        try:
            p.year = data["year"]
        except:
            logger.warning(
                f"Couldn't read <year> for {json_file}")
            pass
        try:
            p.abstract = data["abstract"]
        except:
            logger.warning(
                f"Couldn't read <abstract> for {json_file}")
            pass
        try:
            p.sections = data["sections"]
        except:
            logger.warning(
                f"Couldn't read <sections> for {json_file}")
            pass
        try:
            p.references = data["references"]
        except:
            logger.warning(
                f"Couldn't read <references> for {json_file}")
            pass
        try:
            p.issn = data["issn"]
        except:
            logger.warning(
                f"Couldn't read <issn> for {json_file}")
            pass
        try:
            p.doi = data["doi"]
        except:
            logger.warning(
                f"Couldn't read <doi> for {json_file}")
            pass
        return p

    def toDataFrame(self):
        """Returns a pandas DataFrame with the paper data"""
        row = {"journal": [self.journal], "year": [self.year], "doi": [
            self.doi], "paragraph": [""], "section": [""]}
        df = pd.DataFrame(row)

        for section in self.sections:
            for paragraph in section["paragraphs"]:
                a = pd.DataFrame({"journal": [self.journal], "year": [self.year], "doi": [
                    self.doi], "paragraph": [paragraph], "section": [section["title"]]})
                df = df.append(a)

        df = df.iloc[1:, :]
        return df

    def __str__(self):
        return f'Paper({self.url})'
