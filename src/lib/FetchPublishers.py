# import logging first to start logging
from lib import Logger
import logging
# import own modules next

# import general modules
import bs4
from bs4 import BeautifulSoup
import unidecode
import re
from configparser import ConfigParser

# get config info
config = ConfigParser()
config.read("config.ini")

# start the logger
logger = logging.getLogger(__name__)
logger.addHandler(Logger.start_log_streaming())


# use super() to refer to functions form the parent class


class Mixin():
    """Scrape functions are stoed in this class."""

    def fetchContentOUP(self, soup: bs4.BeautifulSoup) -> None:
        """Get the content from the HTML file using Beautiful SOUP."""
        logger.debug("fetchContentOUP is called.")

        # Save authors
        try:
            self.authors = []
            authors = soup.find("div", class_="at-ArticleAuthors").find("div",
                                                                        class_="al-authors-list").find_all("a", class_="linked-name")
            for a in authors:
                self.authors.append(a.text)
        except:
            logger.warning(f"Couldn't find authors for url: {self.doi}")

        # Save references
        try:
            self.references = []
            refs = soup.find_all("div", class_="crossref-doi")
            for r in refs:
                self.references.append(r.find("a")["href"])
        except:
            logger.warning(f"Couldn't find references for url: {self.doi}")

        # Save abstract
        try:
            results = soup.find(id="ContentTab")
            self.abstract = results.find("section", class_="abstract").text
        except:
            logger.warning(f"Couldn't find abstract for url: {self.doi}")

        logger.debug(
            f"Authors, references, and abstract are saved.")
        # Save sections and paragraph
        try:
            self.sections = []
            content = results.find("div", class_="widget-items")

            # Remove all tables
            tables = content.find_all(class_="table-wrap")
            tables = tables + \
                content.find_all(class_="table-full-width-wrape")
            tables = tables + content.find_all(class_="table-wide")

            for t in tables:
                t.decompose()

            num = -1
            for i, c in enumerate(content.children):
                if c.name == "h2" and "section-title" in c["class"]:
                    num = num + 1
                    self.sections.append(
                        {"title": c.text, "paragraphs": []})
                if c.name == "p" and "chapter-para" in c["class"]:
                    self.sections[num]["paragraphs"].append(
                        unidecode.unidecode(c.text))
                else:
                    continue
            logger.debug(
                f"Sections and paragraphs are saved.")
        except:
            logger.warning(
                f"Couldn't find sections and paragraphs for url: {self.doi}")

    def fetchContentSAGE(self, soup: bs4.BeautifulSoup) -> None:
        """Scrapes with SAGE specific extraction."""
        # Get HTML soup

        # ---- start extraction of manuscript data --- #
        logger.info(f"Start extraction of manuscript data for {self.doi}")
        # Save authors
        try:
            self.authors = soup.find("meta", property="article:author")[
                "content"].split(sep=",")
        except:
            logger.warning(f"Couldn't find authors for url: {self.doi}")

        # Save references
        try:
            self.references = []
            # delete ending with links
            delete = ["Google Scholar", "ISI", "Crossref",
                      "SAGE Journals", "Medline", "|", "\n"]
            ref_info = soup.find("table", class_="references").find_all(
                "td", valign="top")
            for i in range(0, len(ref_info)):
                ref = ref_info[i].text
                for d in delete:
                    ref = ref.replace(d, "")
                ref = ref.replace(" .", ".")
                self.references.append(ref.strip())
        except:
            logger.warning(f"Couldn't find references for url: {self.doi}")

        # Save abstract
        try:
            self.abstract = soup.find(
                "div", class_="abstractSection abstractInFull").text
        except:
            logger.warning(f"Couldn't find abstract for url: {self.doi}")

        # Save sections and paragraph
        try:
            # self.sections gets the structure:
            # list of dictionairies (one for each section), identified by "title"
            #  -- list of subsections nested in each section, identified by "title"
            #      -- list of paragraphs in each subsection, no identifier, order is order in the manuscript
            self.sections = []
            # this gets the main body of text on a SAGE website
            logger.debug(f"Get main body of content form the manuscript.")
            content = soup.find("div", class_="hlFld-Fulltext")

            # replace figure references with their text
            fig_ref = content.find_all(
                "span", {"class": "figure refFigure figuresContent"})
            for i, f in enumerate(fig_ref):
                # note: reference wird nicht überschrieben, sondern neu gesetzt
                f.replace_with(f.text)

            # replace table refereces wit their text
            tab_ref = content.find_all(
                "a", {"class": "ref showTableEvent"})
            for i, t in enumerate(tab_ref):
                t.replace_with(t.text)

            # delete all other parts of figures and tables from the document
            # build list of references
            fig = content.find_all("script")
            fig = fig + content.find_all("table")
            fig = fig + content.find_all("div", class_="figure")
            fig = fig + content.find_all("div", class_="table")
            fig = fig + content.find_all("a", class_="showFiguresEEvent")
            fig = fig + content.find_all("a", class_="showTableEvent")
            fig = fig + \
                content.find_all(
                    "div", class_="tableWrapper visuallyhidden")

            # delete from object 'con' by references with the deconpose() function
            for t in fig:
                t.decompose()
            logger.debug(f"Replace figures and tables.")

            def extract_paragraph(html_element: 'bs4.element.Tag', sec_index: int, sub_index: int, recurse=True) -> None:
                """This function grabs all paragraphs and writes them in the list of paragraphs of a subsection."""

                paragraphs = html_element.findChildren(
                    "p", recursive=recurse)

                # - loop 3 Paragraphs - #
                for k, p in enumerate(paragraphs):
                    # print(f"Paragraph number {k}")
                    self.sections[sec_index]["subsections"][sub_index]["paragraphs"].append(
                        unidecode.unidecode(p.text))

            # -- SAGE identifies 4 major levels in the main body of the document: -- #
            # sections ending on "level1"
            # subsections ending on "level2"
            # subsubsections: not identified in this code
            # paragraphs: identified by tag "p"

            # sections are extracted
            logger.debug(f"Sections extracted.")
            secs = content.find_all(
                "div", {'class': re.compile("level_1")})

            # the first part (introduction) is not tagged as a section, it needs to be extracted seprately
            self.sections.append(
                {"title": "Introduction", "subsections": []})
            self.sections[0]["subsections"].append(
                {"title": None, "paragraphs": []})
            extract_paragraph(content, 0, 0, recurse=False)

            # three nested loop according to the nesting of the sections object
            # loop 1: over sections (i: section number [+1 because Introduction in inserted above], c: section content)
            #   -- loop 2: over subsections (j: subsection number, sub: subsection content)
            #        -- loop 3: over paragraphs (k: paragraph number, p: paragraph content) --> implemented in the extract_paragraph() function
            # - loop 1 sections - #
            logger.debug(f"Loop over sections.")
            for i, c in enumerate(secs):
                sec_num = i+1
                # print(f"Section number {sec_num}")
                # print("\n")
                # print(c)
                # get the title for the section
                sec_heading = c.findChild("h2", recursive=True)

                if sec_heading:
                    sec_heading = sec_heading.text

                # add section to existing list
                self.sections.append(
                    {"title": sec_heading, "subsections": []})

                # extract subsections
                subs = c.find_all("div", {'class': re.compile("level_2")})
                sub_headings = c.findChildren("div", class_="head-b")
                # sometimes there are no subsection (e.g. Discussion)
                if subs:
                    # - loop 2 Subsections - #
                    for j, sub in enumerate(subs):
                        # print(f"Subsection number {j}")
                        # print("\n")
                        # print(type(sub))

                        sub_heading = sub_headings[j]

                        if sub_heading:
                            sub_heading = sub_heading.text
                        else:
                            sub_heading = f"Subsection {j+1}"

                        self.sections[sec_num]["subsections"].append(
                            {"title": sub_heading, "paragraphs": []})

                        extract_paragraph(sub, sec_num, j)

                else:
                    # If there are no subsections, one artificial subsection is entered and gets the title 'None'
                    sub_heading = None
                    self.sections[sec_num]["subsections"].append(
                        {"title": sub_heading, "paragraphs": []})
                    extract_paragraph(c, sec_num, 0)

        except:
            logger.warning(
                f"Couldn't find sections and paragraphs for url: {self.doi}")
