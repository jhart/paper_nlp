# import logging first to start logging
from lib import Logger
import logging
# import own modules next

# import general modules
from crossref.restful import Works, Journals
import json
import getpass
from configparser import ConfigParser

# start the logger
logger = logging.getLogger(__name__)
logger.addHandler(Logger.start_log_streaming())

works = Works()
journals = Journals()

# get config info
config = ConfigParser()
config.read("config.ini")


class Journal:

    def __init__(self, issn="", title="", papers=[]):
        self.issn = issn
        self.papers = []

        if issn != "":
            journal = journals.journal(issn=self.issn)
            self.title = journal["title"]
        else:
            self.title = title

    def toJson(self):
        try:
            return json.dumps(self, default=lambda o: o.__dict__, indent=4)
        except:
            logger.warning("Couldn't convert to json, returning empty Dict")
            return {}

    def fetchJournalPapers(self, year_start, year_end):
        print("Fetching papers for", self.title)
        self.papers = []
        for year in range(year_start, year_end):
            logger.info(
                f"Getting articles for journal {self.title} for year {year}.")
            for i in works.filter(issn=self.issn, from_pub_date=str(year), until_pub_date=str(year)).sample(100).select('publisher, ISSN, license, URL, issue, volume').sort("published"):
                self.papers.append(i)

    def save(self, filename):
        json_object = self.toJson()

        with open(filename, "w") as outfile:
            outfile.write(json_object)

    def readFile(file):
        logger.debug(f'{config[getpass.getuser()]["folder_journals"]}')
        filename = f'{config[getpass.getuser()]["folder_journals"]}{file}.json'
        logger.info(f"Reading file: {filename}")

        with open(filename) as json_file:
            data = json.load(json_file)
        return data

    def __str__(self):
        return f'Journal({self.issn})'
