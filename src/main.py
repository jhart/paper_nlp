# import logging first to start logging
from lib import Logger
import logging
# import own modules next
from lib import Journals, Paper, Papers
# import general modules
import asyncio

# create requirements
# pip list --format=freeze > requirements.txt


async def main():

    Papers.count()

    year = [2020, 2021]
    # Fetch journal papers
    journals = [
        {"issn": "0266-7215", "years": year},  # ESR
        # { "issn": "0037-7732", "years": [ year, year + 1 ] }, # Social Forces
        # {"issn": "2049-5846", "years": [year, year + 1]},  # Migration Studies
        # { "issn": "0037-7791", "years": [ year, year + 1 ] }, # Social Problems

        # Journal of Health and Social Behavior
        # {"issn": "0022-1465", "years": year},
        # American Sociological Review
        # {"issn": "0003-1224", "years": year},

    ]

    # - LOGGING - #
    # deleting log is wanted
    Logger.delete_logs(all=False)
    logger = logging.getLogger(__name__)

    logger.addHandler(Logger.start_log_streaming())
    logger.info(f"Main call!")

    Journals.fetchAll(journals)
    Journals.savePapers("european_sociological_review")

    # df = Papers.toDataFrame()
    # df.to_csv("data/csv/papers.csv")

    # Papers.createTrainingData("method")

if __name__ == "__main__":
    asyncio.run(main())
