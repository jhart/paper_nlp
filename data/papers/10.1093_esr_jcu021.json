{
    "doi": "http://dx.doi.org/10.1093/esr/jcu021",
    "abstract": "",
    "sections": [
        {
            "title": "Scope",
            "paragraphs": [
                "European Sociological Review publishes articles in all fields of Sociology ranging in length from short research notes to major articles of a maximum of 7,000 words. Comments on articles are invited and authors will be given the opportunity to respond. The book review section will regularly include review articles as well as reviews of individual publications."
            ]
        },
        {
            "title": "Submission of papers",
            "paragraphs": [
                "The Editors will only consider papers written in acceptable English. It will be the sole responsibility of the authors to ensure good linguistic quality. The European Sociological Review can provide some help with English language editing, but a check of the text by a native speaker is strongly recommended. Except in prearranged circumstances submissions of more than 7,000 words will be rejected immediately. All submissions of articles, notes, comments or reviews should be sent via our submission website at http://mc.manuscriptcentral.esr. Full submission instructions can be found on our website at http://www.oxfordjournals.org/our_journals/eursoj/for_authors/submission_online.html.",
                "Books for review should be addressed to: Louis-Andre Vallet, Quantitative Sociology Laboratory, CREST, Timbre J350, 3 avenue Pierre Larousse, 92245-Malakoff Cedex, France, email louis-andre.vallet@wanadoo.fr.",
                "Contributors should state a word-count of the length of their submission. Submissions should be accompanied by the telephone and fax numbers and e-mail address of the corresponding author."
            ]
        },
        {
            "title": "Copyright",
            "paragraphs": [
                "It is a condition of publication in the Journal that authors grant an exclusive licence to Oxford University Press. This ensures that requests from third parties to reproduce articles are handled efficiently and consistently and will also allow the article to be as widely disseminated as possible. As part of the license agreement, authors may use their own material in other publications provided that the Journal is acknowledged as the original place of publication, and Oxford University Press as the publisher."
            ]
        },
        {
            "title": "Preparation of manuscripts",
            "paragraphs": [
                "All material should be typed double spaced, and on one side of the page only. Ample margins should be left. The pages should be numbered serially.",
                "An abstract of the paper of not more than 200 words should be supplied.",
                "Up to two levels of sub- or cross-heads are permitted as a means of dividing up the text and indicating to the reader the structure of the article.",
                "Each article should have a list of references of items cited in alphabetical order in the following style:",
                "Gans, H. J. (1962). The Urban Village: Group and Class in the Life of Italian-Americans. New York: The Free Press.",
                "Gerth, H. H. and Wright Mills, C. (Eds.) (1948). From Max Weber: Essays in Sociology. London: Routledge & Kegan Paul.",
                "Spate,O. H. K. (1952). Toynbee and Huntingdon: A Study in Determinism. The Geographical Journal, 118, 406-428.",
                "References should include only authors' initials, not full first names. Republished works (e.g. classics) should be referred to both by year of original publication and by year of publication of the edition quoted. All citations should then be placed within parentheses in the text: e.g. (Gans, 1962), (Gerth and Mills, 1948, ch. IV), (Spate, 1952: 415-16).",
                "The list of references should be on separate pages.",
                "Notes, when necessary, should be numbered continuously and will be printed as end notes. They should also be typed on separate pages. Acknowledgements should be placed at the end of the Notes.",
                "All artwork (graphs, figures, diagrams, etc.) must be provided by authors in electronic format, as high-resolution (300 dpi) tif, eps, ps or jpgs files.",
                "Articles submitted for publication must not be under consideration for publication elsewhere.",
                "Authors are requested to provide full information on any prior publication in a non-English language.",
                "Reasons for the rejection of articles will only be given if revisions are being recommended."
            ]
        },
        {
            "title": "Offprints",
            "paragraphs": [
                "Authors of full articles will receive a gratis copy of the issue, and the corresponding author will also receive free online access to their article.Offprints of articles may be obtained by authors at their own expense from Oxford University Press."
            ]
        }
    ],
    "references": [],
    "title": "Notes to Contributors",
    "issue": "3",
    "volume": "30",
    "year": 2014,
    "issn": "0266-7215",
    "journal": "European Sociological Review",
    "authors": []
}