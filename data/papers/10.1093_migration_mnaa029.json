{
    "doi": "http://dx.doi.org/10.1093/migration/mnaa029",
    "abstract": "The discourse on climate change and migration has shifted from labelling migration merely as a consequence of climate impacts, to describing it as a form of human adaptation. This article explores the adaptation framing of the climate change and migration nexus and highlights its shortcomings and advantages. While for some groups, under certain circumstances migration can be an effective form of adaptation, for others it leads to increased vulnerabilities and a poverty spiral, reducing their adaptive capacities. Non-economic losses connected to a change of place further challenge the notion of successful adaptation. Even when migration improves the situation of a household, it may conceal the lack of action on climate change adaptation from national governments or the international community. Given the growing body of evidence on the diverse circumstances and outcomes of migration in the context of climate change, we distinguish between reactive and proactive migration and argue for a precise differentiation in the academic debate.",
    "sections": [
        {
            "title": "Introduction",
            "paragraphs": [
                "Projections of climate impacts show that some areas that currently provide livelihoods to subsistence farmers, fishers, or urban dwellers could become uninhabitable in the future. Threats to livelihoods may come in the form of sea level rise, hydrological extremes, tropical cyclones, or heat waves. The expected level of exposure to hazards could mean that people would not be able to maintain a basic standard of living, rendering permanent habitation in some locations technically unfeasible or morally intolerable. In some extreme events, like in the case of the recent wild fires in Australia (temporary), migration can be the only feasible strategy to ensure survival. In part to recognize migration can also serve other ends, the notion of migration as adaptation emerged in the academic literature, highlighting the positive potential of migration to diversify livelihoods. This strand of literature underlines the agency of the migrants and their capacity to proactively respond to hazards. The concept of migration as adaptation also plays a role in Elizabeth Ferris' article, which provides an overview of how different epistemic communities have approached migration in the context of climate change. Ferris differentiates between climate scientists who see migration as an impact of climate change and migration researchers who often take the viewpoint of migration as adaptation. In our contribution to this colloquium, we critically explore how the notion of migration as adaptation evolved, what empirical findings tell us, and in which contexts it matters whether migration is framed as adaptation or as a mere response to survive mounting climate impacts. We emphasize that migration does not necessarily lead to increased adaptive capacities for households in all contexts but can also have detrimental consequences, leading to increased impoverishment and deepened vulnerabilities."
            ]
        },
        {
            "title": "1. The evolvement of the \u2018migration as adaptation\u2019 discourse",
            "paragraphs": [
                "When early research warned of future 'waves' of 'environmental refugees', it prompted discussions about appropriate protection frameworks among scholars and practitioners on the onside (McAdam 2012). In other circles, powerful actors started to point to security implications of climate change, and migration has often been their 'shorthand for climate security concerns in general' (Baldwin et al. 2014: 125). For example, members of the UN Security Council have frequently mentioned migrants in debates on climate security. Several authors have criticized this securitization trend, pointing to the risks of dehumanizing affected populations and legitimizing military approaches (Hartmann 2010; Baldwin et al. 2014; Geddes 2015).",
                "Since the 2000s, the discourse has shifted from highlighting the forced nature of environmental migration and related security threats to emphasizing migration as one possible, proactive adaptation solution that should be governed and facilitated (Piguet 2013). Policy experts (Honarmand Ebrahimi and Ossewaarde 2019), international organizations (Hall 2015), and researchers (Sakdapolrak et al. 2016) have contributed to this shift. One key actor is the International Organization for Migration, which has introduced the concept of 'migration as adaptation' into many practice-oriented discourses (Felli 2013). This gives migration a positive spin as compared to the controversial 'climate refugees' narrative. The reframing of migration as a possible means for adaptation was taken up by major actors like the Intergovernmental Panel on Climate Change (IPCC) and is reflected in strategic documents such as the Cancun Adaptation Framework (UNFCCC 2010), the Sendai Framework for Disaster Risk Reduction (UNISDR 2015), and the Global Compact for Migration (UNGA 2018).",
                "Part of the migration as adaptation argument is that households assess all available options to adjust to hazards and choose the ones best suited to their situation, which can include a conscious decision to migrate, if needed resources are accessible (Bilsborrow 1992; McLeman and Smit 2006; Black et al. 2011; McLeman 2016). Authors see adaptive potentials of migration in generating income, diversifying livelihoods, spreading household risks, and social or financial remittances (Ober and Sakdapolrak 2017). While the new discourse has proven powerful in some contexts, limitations also became evident."
            ]
        },
        {
            "title": "2. Empirical evidence on the diversity of climate-related migrations and their outcomes",
            "paragraphs": [
                "The concept of migration as adaptation suggests a positive relationship between migration and adaptation processes, involving some form of anticipation and planning (Vinke 2019). It emphasizes the proactivity of migration decisions and the migrant's agency in the migration process. However, whether or not households are able to use migration as an adaptation strategy is influenced by a variety of factors, including the migration context and the capacities of the households. Based on empirical evidence, we present here four arguments for a more diverse framing of climate migration.",
                "In practice, migration often takes the form of short-term coping rather than anticipatory adaptation. The degree of choice and agency in migration decisions can seldom be confidently assessed. Migration is rarely the first adaptation choice, especially if it involves the migration of an entire household. Migration encompasses different variations along a continuum between voluntary and forced forms, categories that are nonexclusive and fluid. It is desirable to see migration as one of several adaptation options for migrants and communities (Gemenne and Blocher 2017), but in some situations, there are no other options but to move away from the hazardous environment. In the mountainous regions of Peru, for example, where glaciers are disappearing, long-term local adaptation is nearly impossible for people lacking the skills needed to adjust to the changing environmental conditions (Bergmann et al. forthcoming). In other contexts, households that are unable to migrate may face deteriorating conditions in a progressively unliveable environment. Those who remain are sometimes too few to carry out agricultural work (Bettini and Gioli 2016).",
                "The term 'adaptation' suggests that households succeed in mediating environmental risks. Yet migration can also fail to secure people's livelihoods and result in increased vulnerabilities for the migrants and their families, even if planned over a longer time horizon. This form of ineffective migration is described as erosive or maladaptation in the literature (Warner et al. 2012; Warner and Afifi 2014). Studies from Southeast Asia, for instance, report high levels of migration but no improvements in average levels of household wealth and food security (Jacobson et al. 2019).",
                "Relocations constitute another form of mobility that is not easily captured by the migration as adaption concept. In fact, relocations that may initially have positive outcomes can become maladaptive in the longer term or in other sectors of life. Each case of relocation involves different risks (Correa 2011; De Sherbinin et al. 2011; Wilmsen and Webber 2015; Arnall 2019). For example, government-led community relocations in Fiji and Papua New Guinea (PNG) that initially saved lives and expanded land access were ultimately blighted with community dislocation, loss of cultural heritage and identity, and land disputes (Gharbaoui and Blocher 2018; for PNG, see also Melde et al. 2017). Research shows that relocations can nevertheless result in increased adaptive capacities for some relocatees, for example, when the new location enables them to exercise new livelihoods skills (Gharbaoui and Blocher 2016a).",
                "Even if some hard indicators suggest that migration has led to improved conditions, there can still be negative consequences for emotional well-being, mental health, and other factors that are difficult to measure. Researchers should not overlook the so-called 'non-economic losses' from climate change, such as the disappearance of traditional livelihoods and cultural heritage. For example, island states may see the erosion of their irreplaceable place-based identity due to anthropogenic sea level rise (Gharbaoui and Blocher 2016b; Vinke 2019). Several researchers argue that migration is not 'successful' adaptation if it 'results in damage to people's traditions, knowledge, social orders, identities, and material cultures' (Adger and Barnett 2005; Adger et al. 2011: 20). In light of the costs of migration, development efforts could also focus on releasing migration pressures to ultimately allow people to stay in their established communities (Upadhyay 2014).",
                "The sections above focus on adaptation of individuals or single households. However, the focus of the IPCC definition of adaptation lies at the systems level: '[adaptation is] the process of adjustment to actual or expected climate and its effects... in human systems, adaptation seeks to moderate or avoid harm or exploit beneficial opportunities'. (IPCC 2014: 5, emphasis added). When migration is framed as adaptation, responsibility is implicitly shifted away from the societal system to the individual or household. This inevitably provokes the moral question: who is responsible for adaptation?",
                "In many cases, migration as adaptation fills a governance void. Communities that cannot rely on sufficient government support or international aid may use remittances to close financing gaps for climate change resilience efforts (Musah-Surugu et al. 2018). In some contexts, when national disaster response and management systems are overwhelmed, host families temporarily absorb people displaced by disasters (Vinke et al. 2020). Overall, governance failures exacerbate inequalities that contribute to disaster displacement risk (Ginnetti 2015).",
                "Existing structural inequalities (re)produce socio-ecological vulnerabilities, which allow some to migrate while forcing others to remain in areas of risk. At the global scale, some migrants are exploited to create and export value through international supply chains. These circumstances reinforce a neoliberal economic regime with ultimately negative consequences for climate protection and socioeconomic equality (Felli and Castree 2012; Felli 2013; Baldwin 2016; Bettini 2017). Used irresponsibly, a broad conceptualization of migration as adaptation can be a fig leaf for governmental inaction rather than an effective strategy to minimize harm. Altogether, there is a risk of mislabeling migration as adaptation if it is precipitated by structural inequalities imposed by economic systems, politics of neglect, and climate change.",
                "Taken together, our four central arguments suggest a need for a more diverse framing of climate migration. Figure 1 shows different key concepts, terminologies, and classifications used in the migration literature, which are useful in this regard. The illustration distinguishes between typically more proactive forms of migration, which involve an anticipatory risk assessment and a preventive decision to migrate, and survival migration as a mere impact response after the occurrence of an environmental shock (Betts 2013; Vinke 2019). The latter type of migration is common after rapid-onset hazards, such as disasters, which impose an immediate threat and displace populations. More proactive, planned forms of migration can be effective or ineffective, which we subsume under the terms adaptive migration and maladaptive migration."
            ]
        },
        {
            "title": "3. Conclusion: beyond semantics",
            "paragraphs": [
                "Conceptual and terminological debates around climate change adaptation and migration are still vivid in academic circles. These are relevant--rising populist discourse and brutalization of language toward migrants have contributed to verbal and physical violence. Simply put: words matter. Migration concepts and terms are often not morally neutral or indifferent, and the migration as adaptation discourse has to be grounded in considerations of justice. When framing migration as adaptation, it has to imply that the affected person can live equally well or better after their migration. In situations in which migrants fall into poverty traps or experience cultural loss, the adaptation label should not be used as a smoke screen to hide governance failures that led to human suffering. Therefore, using appropriate wording is key to both science and policy.",
                "Beyond the discursive debates, research with human subjects like on migration should benefit human development and the protection of those who are most vulnerable. As the climate crisis escalates, research funds should be directed toward finding solutions to enable migration that could support effective adaptation while addressing often overlooked noneconomic losses and pressures on well-being. This will necessarily have to consider systems as a whole, with different actors and from different angles. Such research would serve three main goals: strengthening the ability to plan and anticipate; opening a range of options available to the individual; and identifying pathways to successful outcomes for migrants, sending and receiving communities."
            ]
        },
        {
            "title": "Funding",
            "paragraphs": [
                "The authors acknowledge funding from the EPICC (East Africa Peru India Climate Capacities) project which is part of the International Climate Initiative (IKI). The German Federal Ministry for the Environment, Nature Conservation and Nuclear Safety (BMU) supports this initiative on the basis of a decision adopted by the German Bundestag.",
                "Conflict of interest statement. The authors declare no conflict of interest."
            ]
        }
    ],
    "references": [
        "http://dx.doi.org/10.1038/436328c",
        "http://dx.doi.org/10.1162/GLEP_a_00051",
        "http://dx.doi.org/10.1080/17565529.2018.1442799",
        "http://dx.doi.org/10.1111/tran.12106",
        "http://dx.doi.org/10.1080/21624887.2014.943570",
        "http://dx.doi.org/10.1111/gpol.2017.8.issue-S1",
        "http://dx.doi.org/10.1080/21632324.2015.1096143",
        "http://dx.doi.org/10.1038/478477a",
        "http://dx.doi.org/10.1596/27383",
        "http://dx.doi.org/10.1126/science.1208821",
        "http://dx.doi.org/10.1080/13563467.2012.687716",
        "http://dx.doi.org/10.1068/a44680",
        "http://dx.doi.org/10.1080/09662839.2015.1028191",
        "http://dx.doi.org/10.1111/geoj.2017.183.issue-4",
        "http://dx.doi.org/10.1162/GLEP_a_00299",
        "http://dx.doi.org/10.1002/jid.v22:2",
        "http://dx.doi.org/10.3390/socsci8070214",
        "http://dx.doi.org/10.1007/s10113-018-1387-6",
        "http://dx.doi.org/10.1093/acprof:oso/9780199587087.001.0001",
        "http://dx.doi.org/10.1007/s10584-005-9000-7",
        "http://dx.doi.org/10.1108/IJCCSM-03-2017-0054",
        "http://dx.doi.org/10.1111/geoj.2017.183.issue-4",
        "http://dx.doi.org/10.1080/00045608.2012.696233",
        "http://dx.doi.org/10.1080/17565529.2013.835707",
        "http://dx.doi.org/10.1016/j.geoforum.2014.10.016"
    ],
    "title": "Migration as Adaptation?",
    "issue": "4",
    "volume": "8",
    "year": 2020,
    "issn": "2049-5838",
    "journal": "Migration Studies",
    "authors": [
        "Kira Vinke",
        "Jonas Bergmann",
        "Julia Blocher",
        "Himani Upadhyay",
        "Roman Hoffmann"
    ]
}