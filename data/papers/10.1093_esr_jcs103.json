{
    "doi": "http://dx.doi.org/10.1093/esr/jcs103",
    "abstract": "",
    "sections": [
        {
            "title": "Silke Aisenbrey",
            "paragraphs": [
                "Silke Aisenbrey is an associate professor of sociology at Yeshiva University in New York. She conducted research as a postdoctoral associate at the Center for Research on Inequality and the Life Course (CIQLE) at Yale University and obtained her PhD at the Ludwig-Maximilians University at Munich. Her research interests lie in the areas of social inequality, welfare states, the life course, gender inequality, and sociology of education. She works mainly with quantitative methods, with a particular interest in longitudinal data analysis."
            ]
        },
        {
            "title": "Erik Bihagen",
            "paragraphs": [
                "Erik Bihagen is an associate professor of Sociology at Stockholm University. He is the coordinator of a large research project using registry data to study social stratification dynamics (SUNSTRAT). His research interests cover inter- and intragenerational mobility, e.g. gender and class origin differences in careers, and the impact of skills and personality."
            ]
        },
        {
            "title": "Erzs\u00e9bet Bukodi",
            "paragraphs": [
                "Erzsebet Bukodi is University Lecturer in Quantitative Social Policy and Professorial Fellow of Nuffield College, University of Oxford. She previously worked as Research Director of the National Child Development Study and the 1970 British Cohort Study in Centre for Longitudinal Studies at the Institute of Education, University of London. Her research interests include: educational inequalities and social mobility; different aspects of life-course analysis; labour market flexibility and family formation and dissolution."
            ]
        },
        {
            "title": "Andreas Diekmann",
            "paragraphs": [
                "Andreas Diekmann is Professor of Sociology at the Swiss Federal Institute of Technology, ETH-Zurich. His research interests focus on theories of social cooperation, experimental game theory, research methods and statistics, and environmental and population sociology. His publications appeared in Proceedings of the Royal Society B, the Journal of Conflict Resolution, Sociological Methods & Research and the American Sociological Review. He is currently conducting research on social norms and institutions, and on environmental protests in China."
            ]
        },
        {
            "title": "Jeffrey C. Dixon",
            "paragraphs": [
                "Jeffrey C. Dixon is an assistant professor of sociology at the College of the Holy Cross, with research interests in political sociology, race/ethnicity, social stratification, and research methods. His previous research has appeared in such journals as The British Journal of Sociology, Social Forces, Public Opinion Quarterly, International Migration Review, and Social Science Quarterly. His current research focuses on European Union (EU) enlargement attitudes, racial prejudice in the United States, and worker insecurity in the EU."
            ]
        },
        {
            "title": "Malcolm Fairbrother",
            "paragraphs": [
                "Malcolm Fairbrother (PhD from the University of California, Berkeley) is Lecturer in Global Policy and Politics in the School of Geographical Sciences at the University of Bristol, and is an affiliate of the Cabot Institute. He has research interests in the areas of political economy, human-environment relations, inequality, globalisation, and methods."
            ]
        },
        {
            "title": "Anette Eva Fasang",
            "paragraphs": [
                "Anette Eva Fasang is assistant professor of Sociology at Humboldt-University Berlin and head of the project group Demography and Inequality the Berlin Social Science Center (WZB). She obtained her Ph.D. from Jacobs University Bremen and did postdoctoral research at Yale University and Columbia University. Her research interests include social demography, stratification, life course sociology, family demography, and methods for longitudinal data analysis."
            ]
        },
        {
            "title": "Andrew S. Fullerton",
            "paragraphs": [
                "Andrew S. Fullerton is associate professor of sociology at Oklahoma State University. His research interests include work and occupations, political sociology, social stratification, and quantitative methods. His work has been published in journals such as Social Forces, Social Problems, Sociological Methods & Research, Public Opinion Quarterly and Social Science Research. He is currently working on several projects related to the causes and consequences of perceived job insecurity in the United States and the European Union."
            ]
        },
        {
            "title": "Jonathan Gershuny",
            "paragraphs": [
                "Jonathan Gershuny is the Professor of Economic Sociology and a former Head of the Oxford Department of Sociology, and a Senior Research Fellow of Nuffield College. He is the Director of the Centre for Time Use Research (funded by the UK Economic and Social Research Council). He was previously the Director of the Institute for Social and Economic Research at the University of Essex (1993 to 2006), and the Principal Investigator of the British Household Panel Study. He was elected as a Fellow of the British Academy in 2002, and chaired its Sociology, Social Statistics and Demography Section in 2006-2008."
            ]
        },
        {
            "title": "Nathalie Giger",
            "paragraphs": [
                "Nathalie Giger is a senior research associate at the Department of Political Science, University of Zurich and a fellow of the Mannheim Centre for European Social Research. Her research interests lay in the linkage between citizens and political elites, in particular in the electoral consequences of social policy and political representation. Her work has appeared in the European Journal of Political Research, West European Politics and European Sociological Review among others."
            ]
        },
        {
            "title": "Tale Hellevik",
            "paragraphs": [
                "Tale Hellevik, Ph.D., is senior researcher at Norwegian Social Research (NOVA) in Oslo. Her Ph.D. topic was the transition to adulthood in Norway, its timing and structure in a historic and comparative perspective, and the importance of financial assistance from parents despite comprehensive public welfare provision. She is currently a post doc within the LIFETIMING project, supported by the HumVIB programme of the European Science Foundation, which is concerned with understanding variations in the organization of the life course in Europe through cultural analyses. She is also coordinator for Nordic Centre of Excellence: Reassessing the Nordic Welfare Model."
            ]
        },
        {
            "title": "Bettina Kohlrausch",
            "paragraphs": [
                "Bettina Kohlrausch is currently working as a senior researcher at the Sociological Research Institute in Goettingen (SOFI). She pursued her PhD at the Bremen International Graduate School of Social Sciences. She has been a visiting PostDoc-Fellow at the Center for Economic Performance at the London School of Economics (LSE) and the European Union Institute (EUI) in Florence. Her recent research focuses on training schemes for low qualified youngsters in Germany--the so-called transition system--and social policy aspects of education."
            ]
        },
        {
            "title": "Oliver Lipps",
            "paragraphs": [
                "Oliver Lipps is head of the methodological research programme at FORS - the Swiss Centre of Expertise in the Social Sciences, Lausanne - and member of the Swiss Household Panel (SHP) team. In addition, he is lecturer in survey methodology and survey research at the Institute of Sociology at the University of Basel. He has published on data quality issues in both cross-sectional and longitudinal designs."
            ]
        },
        {
            "title": "Moira Nelson",
            "paragraphs": [
                "Moira Nelson is an assistant professor (senior lecturer) in political science at Lund University. She conducts research on the nature and determinants of welfare state change and recent work focuses on the politics of human capital investment and the role of voters in governments' reform strategies. Her work has been published in Comparative Political Studies, European Journal of Political Research, and Journal of European Social Policy among others."
            ]
        },
        {
            "title": "Magnus Nermo",
            "paragraphs": [
                "Magnus Nermo is an associate Professor of Sociology at the Swedish Institute for Social Research and the Sociology Department, Stockholm University. He is coordinator of the international research network EQUALSOC. His research interest focuses on gender segregation and stratification in the labour market."
            ]
        },
        {
            "title": "Daniel Oesch",
            "paragraphs": [
                "Daniel Oesch is professor at the Life Course and Inequality Research Centre (LINES) at the University of Lausanne where he lectures on social stratification and labour market policy. His current projects deal with change in the class structure, unemployment and career trajectories in Western Europe. He is the author of Redrawing the Class Map (Palgrave Macmillan 2006) and Occupational Change in Western Europe (Oxford University Press 2013)."
            ]
        },
        {
            "title": "Wojtek Przepiorka",
            "paragraphs": [
                "Wojtek Przepiorka is a post-doctoral research fellow at Nuffield College and the Department of Sociology at University of Oxford. His research interests are in quantitative methodology, in particular experimental methods; analytical sociology; behavioural game theory; and evolution of social cooperation. His work has been published in Proceedings of the Royal Society B, Evolution and Human Behavior, and Journal of Socio-Economics. In his current research, he studies the signalling benefits of altruistic behaviour and mechanisms of social norm enforcement."
            ]
        },
        {
            "title": "Renee Reichl Luthra",
            "paragraphs": [
                "Renee Reichl Luthra is Senior Research Officer at the Institute of Social and Economic Research at the University of Essex. She has published on the labor market integration of various immigrant groups in the United States, and recently completed a study of the educational and labor market outcomes of the children of immigrants in Germany. She is currently part of a cross-national effort to gather data on new immigrants to the United Kingdom, Germany, the Netherlands and Ireland."
            ]
        },
        {
            "title": "Dwanna L. Robertson",
            "paragraphs": [
                "Dwanna L. Robertson, Ph.D. is an Assistant Professor at Kansas State University. Her research focuses on the reproduction of social inequality within the structure of policy, particularly for American Indians, with her current research exploring the negotiation of out-group, in-group, and institutionalized interpretations of Indigenous identity. Dr. Robertson has authored or co-authored in these areas: American Indian Culture and Research Journal, Research in the Sociology of Work; In Our Own Backyard: Human Rights, Injustice, and Resistance in the U.S.; and The Oxford Encyclopedia of American Business, Labor, and Economic History."
            ]
        },
        {
            "title": "Richard A. Settersten",
            "paragraphs": [
                "Richard A. Settersten, Jr., Ph.D., is Hallie Ford Endowed Chair and professor of Social and Behavioral Health Sciences at Oregon State University. He is also a member of the MacArthur Research Network on Transitions to Adulthood. A graduate of Northwestern University's program in Human Development and Social Policy, Settersten has held fellowships at the Max Planck Institute for Human Development and Education in Berlin, the Institute for Policy Research at Northwestern, and the Spencer Foundation in Chicago. Besides MacArthur and Spencer, divisions of the National Institutes of Health in the United States have supported his research."
            ]
        },
        {
            "title": "Heike Solga",
            "paragraphs": [
                "Heike Solga is head of the research unit \"Skill Formation and Labor Markets\" at the WZB and professor of sociology at the Freie Universitat Berlin. She is head of the \"College of Interdisciplinary Educational Research\", a joint initiative of the German Ministry of Education and Science (BMBF), the Jacobs Foundation and the Leibniz Association. She was professor at the University of Gottingen and Leipzig and visiting professor at Yale University and ETH Zurich. She served as director of the SOFI (Gottingen, 2006-2010). Her research interests are life-course research, educational and labor market inequalities with focus on the less educated."
            ]
        },
        {
            "title": "Charlotta Stern",
            "paragraphs": [
                "Charlotta Stern is an associate professor of Sociology at Stockholm University. She coordinates the University's HR-program, and teaches the sociology of organization and labor markets. Her current research focuses on mechanisms behind top position selection."
            ]
        },
        {
            "title": "Federico Varese",
            "paragraphs": [
                "Federico Varese is Professor of Criminology at the University of Oxford. His main research interests are the study of organised crime, corruption, social network analysis, and the dynamics of altruistic behaviour. His publications include The Russian Mafia (2001), Organized Crime (2010), Mafias on the Move (2011) and several journal articles. He is a past Editor of the journal Global Crime."
            ]
        }
    ],
    "references": [],
    "title": "Notes on Contributors",
    "issue": "5",
    "volume": "29",
    "year": 2013,
    "issn": "0266-7215",
    "journal": "European Sociological Review",
    "authors": []
}