{
    "doi": "http://dx.doi.org/10.1177/0022146511402959",
    "abstract": "",
    "sections": [
        {
            "title": "Introduction",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "I am honored to have the opportunity to serve as editor of JHSB. I have been reading JHSB since I was a graduate student, finding much inspiration for my research in its pages. Past editors have done an outstanding job of publishing innovative and quality research. I am committed to continuing this tradition and will not fundamentally alter what JHSB does. However, you will see one new feature in this issue with the introduction of a \"Policy Brief\" series.",
                        "Increasingly, policymakers look to sociologists to provide guidance in promoting population and community health and reducing social disparities in health and health care. Much of the research published in JHSB is directly relevant to these concerns but policymakers are unlikely to carefully read research articles and then draw conclusions for health policy. Beginning with this issue, JHSB will showcase an article and have the author develop a one page brief that policymakers, as well as the media and general public, can look to. This should attract a broader audience to research published in the journal and help link basic sociological research on health to health in \"the real world.\" Briefs will also be widely distributed and posted electronically, insuring easy access to new findings.",
                        "JHSB has long been the major showcase for theoretical advances and groundbreaking research on sociological aspects of health and illness. The high status of JHSB as a publication outlet already goes well beyond sociology--with a strong impact factor as rated by the Institute for Scientific Information and the national media often reporting on research published in the journal. I urge you to help us sustain and advance the high visibility of the journal by submitting your latest work to JHSB. The editorial team values innovation, theoretical grounding, high quality data, and cutting edge methodological approaches in the study of social aspects of health and illness and the organization of medicine and health care. The journal is dedicated to publishing the best research on these general topics. We also encourage submissions on emerging debates and issues including LGBT health, intersectionality, and genetics and biomarkers, as well as the use of mixed methods and qualitative methods.",
                        "Reviewers provide the behind-the-scenes energy and creativity that make every issue of JHSB possible. If you have not reviewed for JHSB in the past, I invite you to send in your vita and tell us about your areas of expertise. If you have not reviewed for JHSB recently, please remind us of your availability. A diverse array of substantive interests and methodological approaches of reviewers insures a high quality and efficient review process.",
                        "My main research interests focus on social ties and health, with attention to mental health, aging and the life course, stress, social support, death and dying, health behavior, gender, and family ties. I have used quantitative and qualitative methods. Our talented and energetic Associate Editors and Deputy Editors represent a diverse range of research interests and methodological approaches. I welcome the new Editorial Board Members and express my gratitude to the outgoing members. New Deputy Editors include Ronald Angel, Mark Hayward, and Robert Hummer at the University of Texas, Stephanie Robert at the University of Wisconsin, and Chloe Bird of the Rand Corporation. To avoid potential conflicts, articles submitted by University of Texas authors will be routed to an external Deputy Editor who will choose reviewers and make editorial decisions on those manuscripts.",
                        "Eliza Pavalko, immediate past editor of JSHB, maintained the highest standards for the journal, oversaw transition to a fully electronic review system, reduced submission to publication lag time, and increased media coverage for the journal. JHSB celebrated its 50th year of publication during Eliza's tenure by producing a special issue of the journal edited by Janet Hankin and Eric Wright. This signature issue highlighted key research findings and policy implications based on a half century of advances in medical sociology.",
                        "I thank Eliza for her leadership, organization, and guidance in the journal's transition from Indiana to Texas. Indiana managing editors, Joseph Wolfe and Indermohan Virk, were also tremendously helpful with the transition. I am pleased to introduce the Texas managing editors, Mieke Thomeer and Corinne Reczek. They deserve a lot of credit for helping to launch JHSB from Texas.",
                        "I look forward to engaging with all of you over the next few years--readers, authors, reviewers, board members, and critics. Together, we shape the direction of JHSB and I expect the next fifty years will be as exciting as the first fifty."
                    ]
                }
            ]
        }
    ],
    "references": [],
    "title": "Editor's Note",
    "issue": "1",
    "volume": "52",
    "year": 2011,
    "issn": "0022-1465",
    "journal": "Journal of Health and Social Behavior",
    "authors": [
        "Debra Umberson"
    ]
}