{
    "doi": "http://dx.doi.org/10.1177/0003122415620404",
    "abstract": "",
    "sections": [
        {
            "title": "Introduction",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "Although this is the first \"official\" issue of our editorship, we have been hard at work since the beginning of July handling new submissions and, more recently, resubmissions. It will be several more issues before the articles we have handled begin to show up in print, as the Vanderbilt editorial team, thankfully, left us with a healthy backlog. This transition period has allowed us to implement some of our new ideas for the journal, while adjusting our initial plans to deal with a rising volume of submissions and still giving each paper the attention it deserves. Indeed, in our first three months we received 171 new submissions (a 29 percent increase over the number of submissions during the same months the year before). Our work on the journal over the past seven months has served as a strong reminder of the importance of American Sociological Review for the discipline.",
                        "ASR is clearly important to the authors who invest so much effort in preparing manuscripts that, they hope, will find a home in the flagship journal of the American Sociological Association. ASR is important to reviewers, who graciously give their valuable time to evaluate the papers and offer helpful comments to improve them--comments that contribute to the development of scholars' ideas, writing, and empirical work, even for those papers that are rejected but ultimately find a home elsewhere. ASR is important to sociologists in the United States who count on it to publish the type of cutting-edge research that will benefit their own scholarship and teaching. ASR is important to sociologists outside of the United States, who are increasingly likely to see the journal as the most desirable outlet for their work (approximately one fourth of new submissions to ASR in 2015 came from outside the United States). Finally, ASR content is becoming \"must-read\" material for scholars outside the discipline, researchers outside the country, non-scholarly or policy-focused audiences interested in social scientific work, and media outlets throughout the world. We are deeply honored to be serving as editors of ASR and we understand the burden that comes with this extraordinary responsibility.",
                        "We can, of course, take no credit for the \"importance\" of ASR. It is our job to build on the impressive work of previous editors while relying on authors and reviewers to help us uphold the journal's high standards in the years ahead. In taking on this task, we recognize the new challenges that must be faced due to changes in academia as well as changes in communication technology. More and more, sociologists and other social scientists are not only facing intense pressure to publish early and often, but also to publish their work in the most visible outlets. Although we must necessarily reject most submissions, as we only have room to publish a small number of articles of sufficient quality to make it through a rigorous peer review process, we feel strongly obligated to handle all submissions fairly and efficiently, while using the resources at our disposal to ensure authors receive constructive feedback on their submissions. Our goal here, at the very least, is to provide all authors with a timely decision on their submissions, along with guidance and commentary that may help them publish their work in another high quality outlet."
                    ]
                }
            ]
        },
        {
            "title": "Front-Loading the Expert Feedback on Submissions",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "We believe we can simultaneously maximize the efficiency of the review process and improve the quality of published manuscripts if we severely reduce, if not completely eliminate, the practice of issuing multiple revise and resubmit (R&R) decisions to authors. To reach this goal, we have developed a practice of \"frontloading\" the expert feedback on our most promising submissions. Members of our editorial board are brought into the process early, to help ensure that authors who receive an R&R decision are getting feedback on their papers that not only identifies potential problems, but also helps the author develop a successful revision strategy in light of all reviewers' comments. We, as editors, provide extensive guidance in decision letters in an effort to help authors effectively revise the paper in one take. Our use of the editorial board members also allows us to minimize the practice of bringing in new reviewers to evaluate revised manuscripts, making it easier for authors to maintain coherence of their papers even in cases where extensive revisions are required. Rather than bringing in new reviewers who, understandably, feel compelled to raise new issues for the authors to consider, our board members are driven to increase the quality of the manuscript while also pushing it toward closure, providing feedback that will help the editors to either reject, accept, or offer conditional acceptance to resubmitted papers. We have experienced very encouraging results thus far implementing these editorial practices, including some enthusiastic feedback from authors who have received the R&R decision. We see this as a core component of our approach into the future."
                    ]
                }
            ]
        },
        {
            "title": "Publishing a Broader Range of Sociology\u2019s Best Scholarship",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "Sociology, as a discipline, is noteworthy in terms of the diversity of methodological tools employed. Yet, historically, that diversity has not been reflected in the pages of ASR, as the vast majority of articles use traditional quantitative techniques. For decades, incoming ASR editors have promised to address the issue but have met with limited success. The problem seems to have two main sources, both having to do with reputational stickiness of the journal. Often, the best qualitative work never finds its way into the ASR review process, as authors perceive that the journal will not be receptive toward their work. When the work is submitted to ASR, reviewers may sense that the paper does not fit the journal, given their understandings of what ASR typically publishes. This generates a predictable self-fulfilling prophesy, one that we know from experience can be hard to break.",
                        "Like editors before us, we are sending out clear signals that we do, in fact, welcome submissions from scholars who utilize methodology that has been underrepresented in the journal, including ethnographic, interview-based, comparative-historical, case-based, and experimental approaches. In addition, we are eager to publish work whose primary contribution is theoretical, as we believe ASR should be the place to showcase fundamental developments in the core ideas that animate the discipline. To that end, we are also practicing a more \"directed\" review process that not only signals our desire to receive strong submissions that span a broad range of methodologies, but also provides guidance to reviewers, especially reviewers who may not use the same methodology, on how they can be most helpful to us in evaluating this work. The goal here is by no means to set a lower bar for work that does not employ traditional quantitative methodology or to constrain the type of review the reviewer writes; the goal is to set the appropriate bar and to offer suggestions for evaluating work that may be outside of one's own area of methodological expertise. We can publish only the strongest work, regardless of its type. Yet we do want to push back against barriers that have, in the past, led both authors and readers to turn to other journals to publish or consume the best research that utilizes underrepresented methodologies or that makes its primary contribution through theory generation rather than theory testing."
                    ]
                }
            ]
        },
        {
            "title": "Staying Relevant to Multiple Audiences",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "Because ASR is the flagship journal of the American Sociological Association, we believe the journal is the natural home for those rare time-sensitive research papers that present groundbreaking empirical work that is relevant to ongoing policy debates or to important theoretical debates in the field. An unnecessarily lengthy review process can diminish the impact of these types of papers and can also result in ASR missing out on opportunities to publish this groundbreaking work and set the public agenda on these issues, as authors turn to other outlets that can more quickly disseminate their findings. Our editorial pre-screening process, in which the editors give significant attention to manuscripts the moment they arrive and before they are sent out for review, will allow us to identify time-sensitive research and to take additional steps to ensure they do not get bogged down in a review process that not only delays publication but also obscures the core contribution with excessive framing aimed primarily at insider scholarly audiences. We will continue the successful practices of the previous editors that have helped bring ASR content to the attention of broader (extra-disciplinary and lay) audiences, such as encouraging authors to develop media abstracts, and we are developing additional strategies to disseminate ASR content more broadly within the discipline."
                    ]
                }
            ]
        },
        {
            "title": "A Team Effort",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "Editing American Sociological Review is certainly a daunting task. Yet we have already developed a deep understanding of how many different people contribute to the journal's success. We have assembled an outstanding group of deputy editors: Elizabeth A. Armstrong, Andrew Gelman, Laura Hamilton, Jennifer Lee, Samuel Lucas, David Schaefer, Florencia Torche, and Mike Vuolo. Additionally, we have a strong group of editorial board members, two thirds of whom are continuing on from the previous editorial regime. We are grateful for these board members' positive and flexible response to changes we have implemented that, in many ways, call on them to invest even more of their time and energy into the journal. We also are grateful to the 22 new board members who accepted our invitation to join the editorial team. Many of them have contributed extraordinary service already--even before their terms officially began.",
                        "We are also benefiting from outstanding contributions from graduate students Bryant Crubaugh, Kevin Estep, and Will Cernanec, who are acting as coordinating editors, along with Paige Ambord and Mary Kate Blake who are serving as editorial associates. We are especially fortunate to have Mara Grynaviski serving as our managing editor--a role in which she has excelled for three consecutive ASR editorial teams. The journal simply could not function were it not for the extraordinary efforts of the ad hoc reviewers who graciously volunteer their time and expertise in exchange for our sincere gratitude and the satisfaction that comes from contributing to the common good. We are grateful to Larry Isaac, Holly McCammon, and the entire Vanderbilt team (especially Laura Dossett and Jonathan Coley) for their generous assistance in making the transition. We also benefited tremendously from advice offered by former editor Vinnie Roscigno. Finally, we would like to thank John McGreevy and Mark Schurr, the Dean and Associate Dean of the College of Arts and Letters at Notre Dame, for providing resources and support that allow us to carry out our work each day, as well as Karen Edwards and the American Sociological Association for guidance and resources provided. We look forward, over the next few years, to facilitating the publication and dissemination of the best our field has to offer."
                    ]
                }
            ]
        }
    ],
    "references": [],
    "title": "Meeting the Challenges of a 21st-Century Flagship Journal",
    "issue": "1",
    "volume": "81",
    "year": 2016,
    "issn": "0003-1224",
    "journal": "American Sociological Review",
    "authors": [
        "Omar Lizardo",
        " Rory McVeigh",
        " Sarah Mustillo"
    ]
}