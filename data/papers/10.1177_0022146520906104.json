{
    "doi": "http://dx.doi.org/10.1177/0022146520906104",
    "abstract": "",
    "sections": [
        {
            "title": "Introduction",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "In Social Causes of Psychological Distress (2003), John Mirowsky and Catherine Ross wrote that there would be little interest in social stratification--systematic inequalities in power, prestige, and resources--if the poor and powerless were as happy, healthy, and fulfilled as the wealthy and powerful. The subfield of medical sociology makes the broader field of sociology meaningful by addressing the inequalities in misery and human suffering that are only assumed in mainstream sociology.",
                        "As we begin a new decade, I am excited to see how medical sociology responds to so many enduring and new social problems, from the opioid epidemic, income inequality, and environmental degradation to gun violence, pharmaceuticalization, and status-based inequalities. Perhaps now more than ever, it is critical for medical sociology to produce knowledge that will (hopefully) be useful in describing, explaining, and solving the misery and human suffering that is so fundamentally tied to these and other important social problems.",
                        "JHSB is central to the sociological study of health, illness, and healing systems because it sets important research agendas and standards for the subfield by publishing the most theoretically and methodologically innovative work. I am honored to lead the flagship journal of American medical sociology with so many outstanding colleagues and deputy editors: Monica Casper (University of Arizona), William Cockerham (University of Alabama at Birmingham and College of William & Mary), Terrence Hill (University of Arizona), Robert Johnson (University of Miami), Hedwig Lee (Washington University in St. Louis), Michael McFarland (Florida State University), Miles Taylor (Florida State University), Miranda Waggoner (Florida State University), and Ming Wen (University of Utah). I will surely count on their diverse expertise to handle a wide range of editorial responsibilities."
                    ]
                }
            ]
        },
        {
            "title": "Current Initiatives",
            "subsections": [
                {
                    "title": "Special Issue (March 2021)",
                    "paragraphs": [
                        "It has been roughly ten years since the last special issue of the journal. Although medical sociology has made substantial contributions to our understanding of the social dimensions of health and health care, it is vital for the development of the field to critically consider our findings, challenges, and future directions. This special issue seeks to identify our key contributions to the sociological study of health, illness, and healing systems (since 2000); engage shortcomings of major research streams in medical sociology; and outline important and innovative directions for the field. I will serve as editor of this special issue with my colleagues at Florida State University: Michael McFarland, Miles Taylor, and Miranda Waggoner."
                    ]
                },
                {
                    "title": "Special Issue (March 2021)",
                    "paragraphs": [
                        "I will reach out to scholars in more mainstream fields of sociology, such as culture, social stratification, and social psychology. There are many scholars who link mainstream sociological concepts with health-related concepts, but many of these scholars feel excluded from JHSB. By encouraging these scholars to submit their work and to subscribe to JHSB, I hope to expand authorship and readership, better integrate our subfield into the broader discipline, and enrich the theoretical and methodological impact of the journal."
                    ]
                },
                {
                    "title": "Special Issue (March 2021)",
                    "paragraphs": [
                        "I will prioritize theoretical applications. One of the most enduring criticisms of medical sociology is our lack of theoretical sophistication. One solution to this problem is to encourage the development of our own theories. This strategy is entirely reasonable. Another more efficient solution is to draw from the rich theoretical traditions of other subfields. Incorporating new theoretical traditions into medical sociology will also help to integrate us into the broader discipline and eventually expand the status and impact of our work."
                    ]
                },
                {
                    "title": "Special Issue (March 2021)",
                    "paragraphs": [
                        "I will prioritize methodological diversity. I am fully committed to expanding the journal's methodological base. I want JHSB to represent the cutting edge of research methodology, including quantitative, qualitative, and mixed methods. I would like to publish methods articles in each of these areas to increase the impact of the journal."
                    ]
                },
                {
                    "title": "Special Issue (March 2021)",
                    "paragraphs": [
                        "I will prioritize biological applications. Due to advances in the study of physiological stress, cellular aging, epigenetics, and gene-environment interactions, biological scientists have moved beyond exclusive models of biological determinism to acknowledge that biological processes and socio-environmental conditions often depend on each other. For the most part, sociologists have ignored developments in the biological sciences and have sustained a \"nurture fortress\" to defend against essentialist (now) obsolete notions of biological determinism. The obvious biological links to health-related processes suggest that JHSB should lead the way."
                    ]
                },
                {
                    "title": "Special Issue (March 2021)",
                    "paragraphs": [
                        "I will prioritize intersectional applications. It is no longer sufficient to focus on a single system of social stratification or social inequality. To fully appreciate the diversity in social processes and health-related processes, we must make a concerted effort to examine interactions among multiple systems. Intersectional applications invariably contribute to a more nuanced and theoretically sophisticated understanding of society. More work is needed along these lines to really challenge our taken-for-granted assumptions of theoretical and conceptual invariance."
                    ]
                }
            ]
        },
        {
            "title": "A Note of Thanks",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "Thank you to everyone who has assisted with the editorial transition that began last July. I am extremely grateful to Florida State University for allowing me to pursue this editorial opportunity, particularly Tim Chapin, who is the dean of the College of Social Sciences and Public Policy, and Kathi Tillman, who is the chair of the Department of Sociology. A special thanks to the previous editors, Rich Carpiano and Brian Kelly, for their continued advice and support. Thanks to the team at Purdue, particularly Stephanie Wilson, for training my outstanding managing editor for reviews, Kyle Saunders. A big thanks to Ryan Trettevik and Andrea Polonijo for continuing in their respective roles as managing editor for production and copy editor. Thanks to Ben Dowd-Arrow for serving as editorial assistant. Finally, thank you to Karen Gray Edwards and the American Sociological Association staff for their ongoing support of the journal. In closing, please enjoy the current issue of the journal, and I look forward to reading your submissions!"
                    ]
                }
            ]
        }
    ],
    "references": [],
    "title": "New Problems for a New Decade: A Note from the New Editor",
    "issue": "1",
    "volume": "61",
    "year": 2020,
    "issn": "0022-1465",
    "journal": "Journal of Health and Social Behavior",
    "authors": [
        "Amy M. Burdette"
    ]
}