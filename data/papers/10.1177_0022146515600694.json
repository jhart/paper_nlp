{
    "doi": "http://dx.doi.org/10.1177/0022146515600694",
    "abstract": "",
    "sections": [
        {
            "title": "Introduction",
            "subsections": [
                {
                    "title": null,
                    "paragraphs": [
                        "In the March 2015 issue of the Journal of Health and Social Behavior, we published a paper by Amelia Karraker and Kenzie Latham. Shortly after the print edition was available, the authors contacted JHSB and informed us about an error in their computer code. The coding error was severe, resulted in major changes to the results, and merited a retraction rather than a corrigendum. The paper has since been retracted and is being republished in the current (September 2015) issue of JHSB. The details about the nature of the error can be found in the section of the journal accompanying the republished article.",
                        "Cases of scientific errors and misconduct surface regularly (Wagner and Williams 2011). The research environment is fast-paced given the ethos to \"publish or perish,\" or in the case of some institutions, \"get grants or get out.\" The bar keeps rising as noted by the increasing number of publications among new assistant professors in sociology (Bauldry 2013) and elsewhere.",
                        "Further, research is becoming increasingly complex, with greater calls for transdisciplinary collaborations, \"big data,\" and more sophisticated research questions and methods. Anyone who has worked with large data sets, such as the Health and Retirement Study (HRS) used by Karraker and Latham, knows how complicated they can be. These data sets often have multiple files that require merging, change the wording of questions over time, provide incomplete codebooks, and have unclear and sometimes duplicative variables. Such complexities are commonplace among many data systems (e.g., National Longi-tudinal Surveys of Youth, National Health and Nutrition Survey, National Co-Morbidity Replication Survey).",
                        "Given these issues, I would not be surprised if coding errors were fairly common, and that the ones discovered constitute only the \"tip of the iceberg.\" If so, such errors may contribute to some of the conflicting findings found in many areas of research. We would hope that most of these errors are inconsequential, although that is not always the case, as shown in the Karraker and Latham study.",
                        "One tool for mitigating scientific errors is replication. Indeed, use of replication seems to be increasing as seen in the trends for studies to release their computer code and data, and in efforts such as the Reproducibility Project: Psychology (https://osf.io/ezcuj/wiki/home/).",
                        "In the present case, Drs. I-Fen Lin and Susan Brown's (Bowling Green State University) attempts to replicate Karraker and Latham's research led to the discovery of the coding errors. Karraker and Latham double-checked their analysis, quickly communicated the problem to JHSB, provided the Stata \".do\" files, and responded thoroughly to all of our queries and critiques. Their republished article was reviewed carefully by several senior members of the JHSB editorial board. Such integrity and diligence stand in stark contrast to other cases that do not appear so forthright (Singal 2015). In short, I want to commend Karraker and Latham for adhering to, in my opinion, a high ethical and professional standard.",
                        "As noted by mathematician and Nobel laureate Frank Wilczek, \"If you don't make mistakes, you're not working on hard enough problems.\" Let us be unafraid to work on problems that can yield mistakes, and let us work collectively to fix errors."
                    ]
                }
            ]
        }
    ],
    "references": [
        "Bauldry, Shawn 2013. \u201cTrends in the Research Productivity of Newly Hired Assistant Professors at Research Departments from 2007 to 2012.\u201d American Sociologist 44(3):282\u201391.",
        "Karraker, Amelia, Latham, Kenzie. Forthcoming. \u201cIn Sickness and in Health? Physical Illness as a Risk Factor for Marital Dissolution in Later Life.\u201d Journal of Health and Social Behavior.",
        "Singal, Jesse 2015. \u201cThe Case of the Amazing Gay-Marriage Data.\u201d New York Magazine. Retrieved July 15, 2015 (http://nymag.com/scienceofus/2015/05/how-a-grad-student-uncovered-a-huge-fraud.html).",
        "Wagner, Elizabeth, Williams, Peter. 2011. \u201cWhy and How do Journals Retract Articles? An Analysis of  Retractions 1988-2008.\u201d J Med Ethics 37(9): 567\u201370."
    ],
    "title": "Editor's Note",
    "issue": "3",
    "volume": "56",
    "year": 2015,
    "issn": "0022-1465",
    "journal": "Journal of Health and Social Behavior",
    "authors": [
        "Gilbert C. Gee"
    ]
}