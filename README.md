# paper_nlp

## What happens here?
1. You provide the script with journal issn's and years
2. The script fetches the journal papers for the years
3. If no information for a paper is stored, the script fetches the paper information from the paper website
4. If the script fetches manuscript text data, it saves the paper in JSON format into data/papers/

## Run Tests
    python -m unittest tests/test_* -v

## Text generation libraries
- https://github.com/minimaxir/gpt-2-simple
- https://github.com/minimaxir/textgenrnn

## Python NLP libraries
- https://nlp.gluon.ai/
  - allows training of models
  - text generation
  - named entity recognition
- https://spacy.io
  - allows training of models  
  - named entity recognition/linking
  - very easy NLP abstraction library that requires little NLP knowledge
- https://keras.io/
  - higher level API for TensorFlow
  - full customization of pipelines, etc.
  - NER: https://keras.io/examples/nlp/ner_transformers/

## Topic classification libraries
- Keras, Tensorflow
- Spacy

## NLP Learning Resources
- https://blog.codecentric.de/en/2020/11/take-control-of-named-entity-recognition-with-you-own-keras-model/
- https://huggingface.co/course/chapter1/1
- https://colab.research.google.com/github/tensorflow/text/blob/master/docs/tutorials/fine_tune_bert.ipynb#scrollTo=jyjTdGpFhO_1

## Which Pretrained NLP Models?
- GPT3
- BERT
- XLNet
- StructBert
- ELECTRA
- DeBERTa

## Text annotation tools
- Open Source
  - https://github.com/planbrothers/ml-annotate
  - https://github.com/doccano/doccano

      docker pull doccano/doccano
      docker container create --name doccano \
        -e "ADMIN_USERNAME=admin" \
        -e "ADMIN_EMAIL=admin@example.com" \
        -e "ADMIN_PASSWORD=password" \
        -p 8000:8000 doccano/doccano

      docker container start doccano

- https://www.lighttag.io/features/named-entity-recognition - free for academics
- https://stanfordnlp.github.io/CoreNLP/index.html
- https://labelstud.io/
- https://github.com/samueldobbie/markup
- https://ubiai.tools/

# ToDo
- Script for data cleaning